<?php

namespace Zegl\Hash;

interface HasherInterface
{
    public function test($test);
    public function hash($input);
}