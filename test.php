<?php

namespace Zegl\Hash;

require_once 'HasherInterface.php';
require_once 'MD5.php';

// Classes to test
$classes = ["\Zegl\Hash\MD5"];

// Strings to test
$tests = [
    'Gustav Westling',
    file_get_contents(__FILE__)
];

foreach ($classes as $class) {
    $hasher = new $class();

    echo "-> $class\n";

    foreach ($tests as $i => $test) {
        echo "--> $i - " . ($hasher->hash($test) === $hasher->test($test) ? 'Ok!' : 'Fail!') . "\n";
    }
}